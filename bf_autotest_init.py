#!/usr/bin/env python3

# ***** BEGIN GPL LICENSE BLOCK ***** #
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Contributor(s): Campbell Barton
#
# ***** END GPL LICENSE BLOCK *****

# <pep8 compliant>

"""
This script runs inside Blender and executes a single test.

Typically its not launched directly by the user (bf_autotest.py calls).
"""
import bpy

# ----------------------------------------------------------------------------
# Handle Arguments
#
def argparse_create_for_task():
    # note: this isn't likely to be called direct, so OK to skimp on info!
    import argparse

    usage_text = "Executes a single test"

    epilog = "todo"

    parser = argparse.ArgumentParser(description=usage_text, epilog=epilog)

    # matching bf_autotest.py
    parser.add_argument("--config", dest="config_filepath", required=True)
    parser.add_argument("--config-cwd", dest="config_cwd", required=True)
    parser.add_argument("--output", dest="output_path", required=True)

    # not a regex
    parser.add_argument("--test", dest="test_id", required=True)

    return parser


# ----------------------------------------------------------------------------
# Test Modes

def run_test_image_render(args, t):
    # Note, this wont handle file-output composite nodes,
    # we could add that (low priority)
    import os

    scene = bpy.context.scene
    render = scene.render

    output_filepath = os.path.join(os.path.abspath(args.output_path), t["id"] + ".png")
    scene.render.filepath = os.path.abspath(output_filepath)

    # possibly want to make it configurable?
    scene.render.threads_mode = 'FIXED'
    scene.render.threads = 8

    # get predictable output
    for scene in bpy.data.scenes:
        render = scene.render
        render.use_overwrite = True
        render.use_file_extension = False
        render.use_placeholder = False
        render.dither_intensity = 0.0
        render.image_settings.file_format = 'PNG'
        render.image_settings.color_mode = 'RGBA'
        render.image_settings.color_depth = '8'
        render.image_settings.compression = 100
        # we could disable crop too
        render.resolution_percentage = 100

    print("rendering to", output_filepath)
    bpy.ops.render.render(write_still=True)

    return True


def run_test_image_bake(args, t):
    print(t)
    return True


def run_test_text(args, t):
    print(t)
    return True


# ----------------------------------------------------------------------------
# Initialize Settings

def main():

    # we may want to be clever here and log info?
    def abort():
        sys.exit(1)

    import sys
    import os

    # ensure this path is in sys.path
    sys.path.insert(0, os.path.dirname(__file__))

    argv = sys.argv[:]
    argv = argv[argv.index("--") + 1:]

    parser = argparse_create_for_task()
    args = parser.parse_args(argv)

    import bf_autotest_parse_config
    config_ns = bf_autotest_parse_config.parse(args.config_filepath)
    config = config_ns["config"]

    t = None
    for t_iter in config:
        if t_iter["id"] == args.test_id:
            t = t_iter
            break
    if t is None:
        print("Task %r missing, this shouldn't happen, aborting" % args.test_id)
        abort()

    pre = t.get("pre")
    if pre is not None:
        print("Executing task callback %r" % pre)
        try:
            pre()
        except:
            import traceback
            traceback.print_exc()
            print("Test %r 'pre' callback failed, aborting" % args.test_id)
            abort()
        print("...done")

    # switch
    ok = {
        'IMAGE_RENDER': run_test_image_render,
        'IMAGE_BAKE': run_test_image_bake,
        'TEXT': run_test_text,
    }[t["mode"]](args, t)

    if not ok:
        print("Test %r failed!" % args.test_id)
        abort()

if __name__ == "__main__":
    main()

