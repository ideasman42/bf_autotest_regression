#!/usr/bin/env python3

# ***** BEGIN GPL LICENSE BLOCK ***** #
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Contributor(s): Campbell Barton
#
# ***** END GPL LICENSE BLOCK *****

# <pep8 compliant>

"""
Example use:

   bf_autotest.py --config=my_test.py --input=my_input/path --output=my_output/path --test="render_sphere"
"""


def argparse_create():
    import argparse

    # When --help or no args are given, print this help
    usage_text = "Execute this script to run Blender over a number of pre-defined regression tests."

    epilog = "Check the output directory after execution"

    parser = argparse.ArgumentParser(
            description=usage_text,
            epilog=epilog,
            )

    parser.add_argument("--config", dest="config_filepath",
            metavar='FILE', required=True,
            help="Configuration file to load tests from",
            )
    parser.add_argument("--config-cwd", dest="config_cwd",
            metavar='DIR', required=False,
            help=("The current-working-directory to use when resolving config paths "
                  "(file paths in the config directory will be accessed relative to this), "
                  "use to avoid referencing absolute paths."),
            )
    parser.add_argument("--output", dest="output_path",
            metavar='DIR', required=True,
            help="Directory used to output the report",
            )
    parser.add_argument("--input", dest="input_path", metavar='DIR', required=True,
            help="Directory pointing to the location of test files to compare against.",
            )
    parser.add_argument("--test", dest="test_pattern",
            metavar='TEST_ID_OR_REGEX', type=str, default=".*", required=False,
            help=("Optional argument to run only a single test "
                  "(or a subset using a regular expressions), when omitted all tests will run."),
            )
    parser.add_argument("--report", dest="report",
            action='store_true', required=False,
            help="Generate a HTML report in the '--output' path.",
            )

    # commands
    parser.add_argument("--blender-cmd", dest="blender_bin",
            metavar='PROGRAM', default="blender", required=False,
            help="Location of blender binary (otherwise simply executes 'blender').",
            )
    parser.add_argument("--idiff-cmd", dest="idiff_bin",
            metavar='PROGRAM', default="idiff", required=False,
            help="Location of 'idiff' binary (otherwise simply executes 'idiff').",
            )

    return parser


def run_test(args, t):
    import os

    filepath_blend = t["file"]
    if not os.path.exists(filepath_blend):
        filepath_blend = os.path.join(args.config_cwd, filepath_blend)
    if not os.path.exists(filepath_blend):
        print("Can't find %r or %r" (t["file"], filepath_blend))
        return None

    output_log = os.path.join(os.path.abspath(args.output_path), t["id"] + ".log")
    stdout = open(output_log, 'wb')

    cmd = [
        args.blender_bin,
        "--background",
        "-noaudio",
        filepath_blend,
        "--python", os.path.join(os.path.dirname(__file__), "bf_autotest_init.py"),
        "--",
        "--test=%s" % t["id"],
        "--config=%s" % args.config_filepath,
        "--config-cwd=%s" % args.config_cwd,
        "--output=%s" % args.output_path,
        ]

    # print(" ".join(cmd))

    import subprocess
    if 0:
        exit_code = 0
    else:
        stdout.write(("Running:" + " ".join(cmd) + "\n").encode('ascii'))
        stdout.flush()
        exit_code = subprocess.call(cmd, stdout=stdout, stderr=stdout)

    if exit_code != 0:
        stdout.write(("\n\nEXIT_CODE: %d\n" % exit_code).encode('ascii'))
        print("  Error: %d, see log for details" % exit_code)
    else:
        if t["mode"] == 'IMAGE_RENDER':
            cmd = [
                args.idiff_bin,
                "-fail", "%g" % t["fuzz"],
                os.path.join(args.output_path, t["id"] + ".png"),
                os.path.join(args.input_path,  t["id"] + ".png"),
                ]
            stdout.write(("Running:" + " ".join(cmd) + "\n").encode('ascii'))
            stdout.flush()

            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, output_err = p.communicate()
            status = output.rstrip().endswith(b"PASS")

            stdout.write(output)
            stdout.write(output_err)

            del p, output, output_err
        else:
            # TODO
            pass

    stdout.close()

    print("  Written:", output_log)

    t_info = {
        "log": output_log,
        "blend": filepath_blend,
        "exit_code": exit_code,
        "status": status,
        }
    return t_info


def main():

    import sys
    import os

    parser = argparse_create()
    args = parser.parse_args()

    # -------------------------------------------------------------------------
    # Parse config

    import bf_autotest_parse_config
    config_ns = bf_autotest_parse_config.parse(args.config_filepath)
    config = config_ns["config"]

    import pprint
    pprint.pprint(config_ns)

    # -------------------------------------------------------------------------
    # Setup State

    try:
        os.makedirs(args.output_path, exist_ok=True)
    except:
        if not os.path.isdir(args.output_path):
            import traceback
            traceback.print_exc()

            print("Failed to create directory %r, aborting" % args.output_path)
            return

    # run tests
    import re
    # print("%r" % args.test_pattern)
    tests = []
    re_test_pattern = re.compile(args.test_pattern)
    for t in config:
        if not re_test_pattern.match(t["id"]):
            print("  Skipping %r" % t["id"])
            continue
        tests.append(t)

    print("-" * 80)
    print("Running %d tests:\n" % len(tests))
    tests_result = {}
    for i, t in enumerate(tests):
        print("  Running %r %d of %d" % (t["id"], i + 1, len(tests)))
        tests_result[t["id"]] = run_test(args, t)
    print("Complete!")

    if args.report:
        inpit_path_relative = os.path.relpath(args.input_path, args.output_path)
        import bf_autotest_report
        output_html = os.path.join(args.output_path, "index.html")
        bf_autotest_report.write(output_html, inpit_path_relative, tests, tests_result)


if __name__ == "__main__":
    main()

