
# override scene defaults (pre-run before rendering)
def pre():
    import bpy
    # set render resolution
    for scene in bpy.data.scenes:
        scene.render.resolution_x = 480
        scene.render.resolution_y = 320


config = [
    dict(id="render_motor9", file="rendering/motor9.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
    dict(id="render_monkey_cornelius", file="rendering/monkey_cornelius.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
    dict(id="render_teapot_soft", file="rendering/teapot_soft.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
    dict(id="render_vectorblur", file="rendering/vectorblur.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
    dict(id="render_windows_tra_shadow", file="rendering/windows_tra_shadow.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
    ]

