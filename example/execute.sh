#!/bin/sh
../bf_autotest.py --help

# note: config-cwd is system specific

../bf_autotest.py \
	--config=config.py \
	--config-cwd="/dsk/src/lib/tests" \
	--input=test_input \
	--output=test_output \
	--test=".*" \
	--report


