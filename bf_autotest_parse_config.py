# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Contributor(s): Campbell Barton
#
# ***** END GPL LICENSE BLOCK *****

"""
The configuration is a Python script,
parsing just validates it and reports errors.
"""

def parse(filepath):

    def run_py(filepath):
        run_globals = {}
        run_locals = {}
        exec(compile(open(filepath).read(), filepath, 'exec'), run_globals, run_locals)
        # only vars defined in this code
        return run_locals

    config_ns = run_py(filepath)
    config = config_ns.get("config")

    if config is None:
        raise Exception("%r doesn't define 'config'" % filepath)

    mode_items = (
        'IMAGE_RENDER',
        'IMAGE_BAKE',
        'TEXT',
        )

    # check type direct or tuple: (func, error_msg)
    param_required = dict(
        id=str,
        file=str,
        mode=(lambda a: a in mode_items, "Expected %r" % (mode_items, )),
        )

    param_optional = dict(
        fuzz=float,
        pre=(callable, "Expected callable argument"),
        post=(callable, "Expected callable argument"),
        )

    for t in config:
        for param, is_required in ((param_required, True), (param_optional, False)):
            for p_id, p_type in param.items():
                t_value = t.get(p_id, Ellipsis)
                if t_value is not Ellipsis:
                    if type(p_type) == tuple:
                        # custom check
                        p_check, p_msg = p_type
                        if p_check(t_value):
                            pass
                        else:
                            raise Exception("%r 'config' has %r member of type %r, %s" %
                                            (filepath, p_id, type(t_value), p_msg))

                    else:
                        # simple type comparison
                        if isinstance(t_value, p_type):
                            pass
                        else:
                            raise Exception("%r 'config' has %r member of type %r, expected %r" %
                                            (filepath, p_id, type(t_value), p_type))
                else:
                    if is_required:
                        raise Exception("%r 'config' missing member %r" %
                                        (filepath, p_id))

    uuid = set()
    for t in config:
        t_value = t["id"]
        if t_value in uuid:
            raise Exception("%r 'config' contains duplicate members %r" %
                             (filepath, t_value))
        uuid.add(t_value)
    return config_ns

if __name__ == "__main__":
    raise Exception("this is not intended to run directly!")

