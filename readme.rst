################################
  Blender Regression Automation
################################

This script is to automate regression tests, so they can run unattended.

Which can be used to generate a HTML report, showing side-by-side comparisons.

Assumptions
===========

You have a directory of blend files, you want to render and compare with previous renders using image compairson.

While other kinds of tests can be added, this is the main purpose of this tool for now.

Implementation
==============

The only system spesific part of this tool is the command used to execute the tests which needs to specify the
base-path used by the config file. Since the config file may point to blend files such as the svn repo used to store
Blender's regression tests.


Components
==========

- ``bf_autotest.py`` - main script to execute tests.
- ``config.py`` - config file which lists tests to run.
- **input path** - path used to validate output.
- **output path** - path where test results are stored.


**Internal components:**

*Theres no need to interact with these directly.*

- ``bf_autotest_report.py`` - creates HTML report.
- ``bf_autotest_init.py`` - executes tests with Blender.


Config File Format
==================


.. This shares some general characteristics with the 'System Demo Mode' addon.

.. code-block:: python

   # overrides
   def pre():
       import bpy
       for scene in bpy.data.scenes:
           scene.render.resolution_x = 480
           scene.render.resolution_y = 320

   config = [
       dict(id="my_test", file="./relative/path/my.blend", mode='IMAGE_RENDER', fuzz=0.01, pre=pre),
       dict(id="my_render", file="./relative/path/some_other.blend", mode='IMAGE_BAKE', fuzz=0.01, pre=pre),
       dict(id="my_other_test", file="./relative/path/foo.blend", mode='TEXT', fuzz=0.01, pre=pre),
       ]

Notes:

- ``id`` unique identifier, must be usable in a filename.
- ``file`` can be an absolute or relative path (see ``--config-cwd`` argument for ``bf_autotest.py``)
- ``mode`` the method used to compare *(currently only 'IMAGE_RENDER' is functional)*
- ``fuzz`` how strictly to compare images where 0.0 is strict 1.0 is relaxed.
- ``pre`` function to call before executing the test
  (typically to override settings in the file such as x/y resolution).


TODO
====

- Make use of some logging library.
- Compare image content.

